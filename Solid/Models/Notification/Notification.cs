﻿namespace Solid.Models.Notification
{
    public class Notification
    {
        private IMessenger _messenger;

        public Notification(IMessenger mes)
        {
            _messenger = new Email();
        }

        public void Notify()
        {
            _messenger.Send();
        }
    }

    public class Program
    {
        public static void Main(string[] args)
        {
            Notification notification = new Notification(new Email());
            notification.Notify();
        }
    }
}