﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Solid.Models
{
    public class EmployeeReport
    {
        // <summary>
        /// Тип отчета
        /// </summary>
        public string TypeReport { get; set; }
        /// <summary>
        /// Отчет по сотруднику
        /// </summary>
        public void GenerateReport(IReportable em)
        {
            if (TypeReport == "CSV")
            {
                // Генерация отчета в формате CSV
            }
            if (TypeReport == "PDF")
            {
                // Генерация отчета в формате PDF
            }

            if (TypeReport == "TXT")
            {

            }
        }
    }
}