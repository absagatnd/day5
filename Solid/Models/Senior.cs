﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


// over-engineering

// Project Manager, Business Analyst, Developer

/*
 * 1. Developer
 *      Разработка, программирование, проектирование разработки, Проведение совещаний
 *
 * 2. Business Analyst
 *      Сбор информации, Составление графика разработки, Аналитика данных
 *
 * 3. Project Manager
 *      Разработка плана, Общение с поставщиками, Проведение совещаний
 */

namespace Solid.Models
{
    public interface IEmployee
    {
        bool AddDetailsEmployee();
        bool ShowDetailsEmployee(int id);
    }

    public interface IAdd
    {
        bool AddDetailsEmployee();
    }

    public interface IShow
    {
        bool ShowDetailsEmployee(int id);
    }

    public class Senior : IAdd, IShow
    {
        public bool AddDetailsEmployee()
        {
            throw new NotImplementedException();
        }

        public bool ShowDetailsEmployee(int id)
        {
            throw new NotImplementedException();
        }
    }

    public class Junior : IAdd
    {
        public bool AddDetailsEmployee()
        {
            throw new NotImplementedException();
        }

    }
}